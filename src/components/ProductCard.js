import {Link} from "react-router-dom"
import {Col,Card, Button} from "react-bootstrap";


export default function Productcard({productProp}){

	let {productName,productDescription,productPrice,isActive, _id} = productProp;

	return (
		(isActive)
		?
		<Card className="p-3 mb-3" text-align="center">
		      <Card.Body>
		        <Card.Title>{productName}</Card.Title>
		        	<Card.Subtitle>Description:</Card.Subtitle> 
		        	<Card.Text>{productDescription}</Card.Text>
		        	<Card.Subtitle>Price:</Card.Subtitle>
		        	<Card.Text>PHP {productPrice}</Card.Text>
		      <Button as={Link} to={`/products/${_id}`}>Details</Button>
		      </Card.Body>
		    </Card>
		:
		<>
		</>
	)
}
