import {Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({bannerProp}){
	console.log(bannerProp);
	const {title, content, destination, label} = bannerProp;

	return(
		<Row>
			<Col>
				<h1>Lonely Island Club</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>
	)
};