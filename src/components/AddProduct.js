import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function AddProduct() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();


	const [productName, setProductName] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState(0);

    const [isActive, setIsActive] = useState(false);


	function addProduct(e) {


	    e.preventDefault();

	    fetch(`https://capstone-2-lee.onrender.com/products/addProduct`, {
	    	method: "POST",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    productName: productName,
			    productDescription: productDescription,
			    productPrice: productPrice,

			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Product succesfully Added",
	    		    icon: "success",
	    		    text: `${productName} is now added`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })


	    setProductName('');
	    setProductDescription('');
	    setProductPrice(0);


	}

	useEffect(() => {

        if(productName !== "" && productDescription !== "" && productPrice > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [productName, productDescription, productPrice]);

    return (
    	user.isAdmin
    	?
			<>
		    	<h1 className="my-5 text-center">Add a Product</h1>
		        <Form onSubmit={(e) => addProduct(e)}>
		        	<Form.Group controlId="productName" className="mb-3">
		                <Form.Label>Product Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Product Name" 
			                value = {productName}
			                onChange={e => setProductName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="productDescription" className="mb-3">
		                <Form.Label>Product Description</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                placeholder="Enter Course Description" 
			                value = {productDescription}
			                onChange={e => setProductDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="productPrice" className="mb-3">
		                <Form.Label>Product Price</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Product Price" 
			                value = {productPrice}
			                onChange={e => setProductPrice(e.target.value)}
			                required
		                />
		            </Form.Group>

	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Save
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Save
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/products" />
	    	
    )

}