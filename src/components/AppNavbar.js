import {useState, useContext} from "react";
import {Navbar,Nav,Container} from "react-bootstrap";
import {Link} from "react-router-dom";
import UserContext from "../UserContext";
import Logo from "../images/Logo.jpg"

export default function AppNavbar(){

  const {user} = useContext(UserContext)
  console.log(user)

  return(
    <Navbar class="fixed-top" bg="danger" expand="lg">
      <Container>
        <Navbar.Brand >
        <Link to="/">
          <img src={Logo} class="rounded-circle" width="20%"/>
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Products</Nav.Link>
            {/*<Nav.Link as={Link} to="/checkout">Checkout</Nav.Link>*/}
            {(user.id !==null) ?
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              :
              <>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    )
}