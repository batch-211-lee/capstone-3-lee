import {Row, Col, Card} from "react-bootstrap";
import React, { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import Product1 from "../images/Miamivice.jpg";
import Product2 from "../images/Junglebird.jpg";
import Product3 from "../images/Darkstormy.jpg"

export default function Highlights(){

console.log("Highlights working")
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
	};
return (
	<Carousel activeIndex={index} onSelect={handleSelect} className="d-block mw-50 mh-25 p-3" max>
      <Carousel.Item>
        <img
          className="d-block w-100 h-100 p-3"
          src={Product1}
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>Miami Vice</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 h-100 p-3"
          src={Product2}
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Jungle Bird</h3>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 h-100 p-3"
          src={Product3}
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Dark and Stormy</h3>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>

	)
}
