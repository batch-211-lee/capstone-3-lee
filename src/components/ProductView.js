import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductView (){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [quantity, setQuantity] = useState("");
	const [productName,setProductName] = useState("");
	const [productDescription, setProductDescription] = useState("");
	const [productPrice, setProductPrice] = useState(0);
	// const [totalAmount, setTotalAmount] = useState("");


	const checkout = (productId) =>{
		console.log("Event triggered")
		fetch(`https://capstone-2-lee.onrender.com/users/checkout`,{
			method:"POST",
			headers:{
				"Content-Type":"application/json",
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				// totalAmount: totalAmount,
				productId: productId,
				quantity: quantity
			})
		})
		.then(

				Swal.fire({
				  title: "Order Added!",
				  icon: "success",
				  text: "Thank you for ordering!"
				}));
				navigate("/products")
			}



	useEffect(()=>{
		fetch(`https://capstone-2-lee.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setProductName(data.productName);
			setProductDescription(data.productDescription);
			setProductPrice(data.productPrice)
		});

	}, [productName,productDescription,productPrice,productId])


console.log(productId, quantity)
 	return (
		<Container className="mt-5 sunsetRed">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
		      			<Card.Body>
		        		<Card.Title>{productName}</Card.Title>
		        		{/*<img src= {`../images/${productName}.jpg`}/>*/}
		        		<Card.Subtitle>Description:</Card.Subtitle> 
		        		<Card.Text>{productDescription}</Card.Text>
		        		<Card.Subtitle>Price:</Card.Subtitle>
		        		<Card.Text>PHP {productPrice}</Card.Text>
		        		<Card.Text>Quantity:</Card.Text>
		        		<input type ="number" name="clicks" value={quantity} onChange={(e)=>setQuantity(e.target.value)}/>
		        		<br/>
		        		{
		        			(user.id!==null && user.isAdmin === false)?
		      				<Button variant="primary" onClick={()=>checkout(productId)}>Order</Button>
		      				:
		      				<Link className="btn btn-danger" to="/login" >Cannot place order</Link>
		      			}
		      			</Card.Body>
		    		</Card>
		    	</Col>
		    </Row>
		</Container>
	)

}