import {Container} from "react-bootstrap";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Products from "./pages/Products";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Checkout from "./pages/Checkout"
import Dashboard from './components/Dashboard';
import AddProduct from './components/AddProduct';
import EditProduct from "./components/EditProduct";
import ProductView from "./components/ProductView"

import {useState, useEffect} from 'react';
import {UserProvider} from "./UserContext"
import './App.css';

function App() {
    const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser=()=>{
    localStorage.clear()
  };

  useEffect(()=>{
    fetch(`https://capstone-2-lee.onrender.com/users/all`,{
      method: "GET",
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
 

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])

    useEffect(()=>{
    fetch(`https://capstone-2-lee.onrender.com/products/all`,{
      method: "GET",
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data);
    })
  },[])

  return (

    <UserProvider value = {{user,setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
            <Container>
              <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/admin" element={<Dashboard/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="/addProduct" element={<AddProduct/>}/>
                <Route path="/editProduct/:productId" element={<EditProduct/>}/>
                <Route path="/products/:productId" element={<ProductView/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path = "/logout" element={<Logout/>}/>
                <Route path = "/checkout" element={<Checkout/>}/>
                <Route path = "*" element={<Error/>}/>
              </Routes>
            </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
