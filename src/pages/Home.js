import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import {Container} from "react-bootstrap";

export default function Home(){
	const data = {
		title: "Lonely Island Bar",
		content: "Enjoy some deliscious cocktails",
		destination: "/products",
		label: "order Now"
	}
	return(
	<>
		<Container className="d-flex align-items-center justify-content-center text-center sunsetTop ">
			<Banner bannerProp={data}/>
		</Container>
		<Container className="d-flex align-items-center justify-content-center text-center sunsetTop ">
			<Highlights/>
		</Container>
	</>
	)
};