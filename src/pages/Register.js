import {Form, Button} from "react-bootstrap";
import {useState,useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate} from "react-router-dom";
import Swal from 'sweetalert2';


export default function Register() {
  const [firstName, setFirstName] =useState("");
  const [lastName, setLastName] =useState("");
  const [email, setEmail] =useState("");
  const [userName, setUserName] = useState("");
  const [mobileNumber, setMobileNumber] =useState("");
  const [password1, setPassword1] =useState("");
  const [password2, setPassword2] =useState("");

  const navigate = useNavigate();

  const [isActive, setIsActive] =useState("");
  const {user, setUser} = useContext(UserContext);

console.log(firstName);
console.log(lastName);
console.log(email);
console.log(mobileNumber);
console.log(userName);
console.log(password1);
console.log(password2);

function registerUser(e){

  e.preventDefault();
  
  fetch(`https://capstone-2-lee.onrender.com/users/register`,{
  	method:"POST",
    headers:{
    	'Content-Type':'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              userName: userName,
              mobileNumber: mobileNumber,
              password: password1
            })
          })
.then(res=>res.json())
.then(data=>{
  setFirstName("");
  setLastName("");
  setEmail("");
  setMobileNumber("");
  setPassword1("");
  setPassword2("");

  Swal.fire({
    title: "Registration successful!",
    icon: "success",
    text: "Welcome!"
    })
  navigate("/login")
})
}

useEffect(()=>{
  if((firstName !=="" && lastName !== "" && mobileNumber.length === 11 && email !== "" && userName!== ""&& password1 !== "" && password2 !=="")&&(password1===password2)){
    setIsActive(true)
  }else{
    setIsActive(false)
  }
},[firstName,lastName,email,userName,mobileNumber,password1,password2])

  return (
      (user.id !== null)
    ?
    <Navigate to="/courses"/>
    :
    <Form onSubmit={(e)=>registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name:</Form.Label>
        <Form.Control type="text"
        placeholder="First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}        
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control type="text" placeholder="Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email}
        onChange={e=>setEmail(e.target.value)}
        required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="userName">
        <Form.Label>Username</Form.Label>
        <Form.Control type="text" placeholder="username"
        value={userName}
        onChange={e=>setUserName(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile No.</Form.Label>
        <Form.Control type="text" placeholder="+63xxx xxx xxxx"
        value={mobileNumber}
        onChange={e=>setMobileNumber(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password"
        value={password1}
        onChange={e=>setPassword1(e.target.value)}
         required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
          />
      </Form.Group>

      {isActive ?
      <Button variant="success" type="submit" id="submitBtn">
        Submit
      </Button>
      :
      <Button variant="danger" type="submit" id="submitBtn" disabled>
        Submit
      </Button>
      }
    </Form>
  );
};

