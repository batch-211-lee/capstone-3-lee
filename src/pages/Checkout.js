import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Checkout(){
	const { user } = useContext(UserContext);


	let product = user.products
	

	const retrieveOrderDetails =() =>{
		fetch(`https://capstone-2-lee.onrender.com/users/${user.id}/checkout`,{
		method: "GET",
		headers: {
			"Content-Type" : "application/json",
			Authorization : `Bearer ${localStorage.getItem("token")}`
			},
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
		})
}
	console.log(JSON.stringify(product))
	console.log(product[0]._id)
	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
		      			<Card.Body>
		        		<Card.Title>productName</Card.Title>
		        		<Card.Subtitle>Description:</Card.Subtitle> 
		        		<Card.Text>productDescription</Card.Text>
		        		<Card.Subtitle>Price:</Card.Subtitle>
		        		<Card.Text>PHP productPrice</Card.Text>
		        		<Card.Text>Quantity:</Card.Text>
		        		{/*<input value=quantity/>*/}
		      				<Button variant="primary" >Checkout</Button>
		      			</Card.Body>
		    		</Card>
		    	</Col>
		    </Row>
		</Container>
	)
}