import {Form, Button} from "react-bootstrap";
import {useState,useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {
	const [userName, setUserName] =useState("");
	const [password, setPassword] =useState("");
	const [isActive, setIsActive] = useState("");

	console.log(userName);
  	console.log(password);


	const {user, setUser} = useContext(UserContext);

	function authenticate (e){
		e.preventDefault();

		fetch(`https://capstone-2-lee.onrender.com/users/login`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userName: userName,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access)
				retrieveUserDetails(data.access)
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome Back to the Lonely Islands Bar!"
				});
			} else{
				Swal.fire({
					title: "Login Failed",
					icon: "error",
					text: "Check your credentials"
				})
			}
		})

		const retrieveUserDetails =(token)=>{
			fetch(`https://capstone-2-lee.onrender.com/users/all`, {
				headers:{
					Authorization : `Bearer ${token}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				let userDetails = data.filter(row=> row.userName === userName)
				console.log(JSON.stringify(userDetails));
				console.log(userDetails[0]._id)
				setUser({
					id: userDetails[0]._id,
					userName: userDetails[0].userName,
					isAdmin: userDetails[0].isAdmin,
					order:[userDetails[0].orders]
				})
			})
		}

		setUserName("");
		setPassword("");
	 };

	useEffect(()=>{
		if(userName !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [userName,password])

	return (
		(user.id !== null)
		?
		<Navigate to="/products"/>
		:
		<Form onSubmit={(e)=>authenticate(e)}>
			<h1>Login</h1>
			  	<Form.Group className="mb-3" controlId="userName">
        			<Form.Label>Username</Form.Label>
        			<Form.Control type="userName" placeholder="Enter username"
        				value={userName}
        				onChange={e=>setUserName(e.target.value)}
         				required />
      			</Form.Group>

      			<Form.Group className="mb-3" controlId="password">
        			<Form.Label>Password</Form.Label>
        			<Form.Control type="password" placeholder="Password"
        			value={password}
        			onChange={e=>setPassword(e.target.value)}
        			  />
      			</Form.Group>

      			{isActive ?
      				<Button variant="primary" type="submit" id="submitBtn">
       					 Login
      				</Button>
      				:
      				<Button variant="danger" type="submit" id="submitBtn" disabled>
        				Login
      				</Button>
      			}
    		</Form>
		)
}